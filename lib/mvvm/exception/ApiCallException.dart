class ApiException implements Exception{
  final String _message;
  final String _prefix;
  ApiException(this._message, this._prefix);
}

class ApiCallException extends ApiException{
  ApiCallException(String message, String prefix) : super(message, "Api Exception");
}