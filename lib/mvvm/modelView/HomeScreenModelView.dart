import 'package:flutter/cupertino.dart';
import 'package:tast_app/mvvm/exception/ApiCallException.dart';
import 'package:tast_app/mvvm/model/Model.dart';
import 'package:tast_app/mvvm/repository/UniversityRepository.dart';

enum FetchingStatus {SUCCESS, ERROR_FAILURE, LOADING}

class HomeScreenViewModel with ChangeNotifier{
  FetchingStatus status = FetchingStatus.LOADING;
  final UniversityRepository _repository;

  HomeScreenViewModel(this._repository);

  Future<List<UniversityModel>> fetchUniversityList() async{
    List<UniversityModel> universities = [];
    try{
      universities = await _repository.getTheUniversityList();
    } on ApiCallException{
      setStatus(FetchingStatus.ERROR_FAILURE);
      return universities;
    }

    print(universities.length);
    setStatus(FetchingStatus.SUCCESS);
    return universities;
  }


  void setStatus (FetchingStatus value){
    status = value;
    notifyListeners();
  }
}