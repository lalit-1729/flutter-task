import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:tast_app/mvvm/exception/ApiCallException.dart';

class UniversityService{

  Future<http.Response> getUniversityData() async{
    final http.Response response;

    try{
      response = await http.get(Uri.parse("http://universities.hipolabs.com/search?country=United+States"));
    } on SocketException{
      throw ApiCallException("No Internet Connection", "The Error caused due to : ");
    }
    return response;
  }
}