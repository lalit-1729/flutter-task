import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tast_app/HomeScreenItemsList.dart';
import 'package:tast_app/mvvm/exception/ApiCallException.dart';
import 'package:tast_app/mvvm/model/Model.dart';
import 'package:tast_app/mvvm/service/UniversityService.dart';

class UniversityRepository {
  Future<List<UniversityModel>> getTheUniversityList() async {
    UniversityService service = UniversityService();

    http.Response response = await service.getUniversityData();

    try {
      response = await service.getUniversityData();
    } on ApiCallException {
      throw ApiCallException("No Internet Connection", "Error due to:");
    }

    if (response.statusCode != 200) {
      throw ApiCallException("Wrong Status Code", "Error due to:");
    }

    List universitiesJson = jsonDecode(response.body) as List;
    List<UniversityModel> universityModels =
        universitiesJson.map((json) => UniversityModel.fromJson(json)).toList();

    return universityModels;
  }
}
