import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tast_app/HomeScreenItemsList.dart';
import 'package:tast_app/mvvm/modelView/HomeScreenModelView.dart';
import 'package:tast_app/mvvm/repository/UniversityRepository.dart';

void main() {
  runApp(FlutterTask());
}

class FlutterTask extends StatelessWidget {
  const FlutterTask({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider<HomeScreenViewModel>(
            create: (_) => HomeScreenViewModel(
              UniversityRepository(),
            ),
          )
        ],
        child: const HomeScreen(),
      ),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Universities", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),),
        ),
        body: const HomeScreenItemsList());
  }
}
