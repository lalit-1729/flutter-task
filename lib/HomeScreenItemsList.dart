import 'package:azlistview/azlistview.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tast_app/DetailsScreen.dart';
import 'package:tast_app/mvvm/modelView/HomeScreenModelView.dart';

import 'mvvm/model/Model.dart';

class University extends ISuspensionBean {
  final UniversityModel universityModel;
  final String tag;

  University(this.universityModel, this.tag);

  @override
  String getSuspensionTag() {
    return tag;
  }
}

class HomeScreenItemsList extends StatefulWidget {
  const HomeScreenItemsList({Key? key}) : super(key: key);

  @override
  _HomeScreenItemsListState createState() => _HomeScreenItemsListState();
}

class _HomeScreenItemsListState extends State<HomeScreenItemsList> {
  List<University> universitiesData = [];

  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      initItems();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final HomeScreenViewModel homeScreenViewModel =
        Provider.of<HomeScreenViewModel>(context);
    return homeScreenViewModel.status == FetchingStatus.LOADING
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : homeScreenViewModel.status == FetchingStatus.ERROR_FAILURE
            ? const Center(
                child: Text("Something went wrong please try again later."),
              )
            : AzListView(
                physics: const BouncingScrollPhysics(),
                padding:
                    const EdgeInsets.only(top: 5, bottom: 5, left: 5, right: 5),
                itemCount: universitiesData.length,
                data: universitiesData,
                itemBuilder: (BuildContext context, int index) {
                  final University item = universitiesData[index];
                  return _buildNameTile(item);
                },
                indexHintBuilder: (context, hint) => Container(
                  height: 60,
                  width: 60,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey,
                  ),
                  child: Text(
                    hint,
                    style: const TextStyle(fontSize: 24, color: Colors.white),
                  ),
                  alignment: Alignment.center,
                ),
                indexBarMargin:
                    const EdgeInsets.only(top: 50, bottom: 50, right: 5),
                indexBarOptions: const IndexBarOptions(
                  textStyle:
                      TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                  needRebuild: true,
                  indexHintAlignment: Alignment.centerRight,
                  indexHintOffset: Offset(-20, 0),
                  selectTextStyle: TextStyle(fontWeight: FontWeight.bold),
                  selectItemDecoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.black26),
                ),
              );
  }

  Widget _buildNameTile(University item) {
    final offsatge = !item.isShowSuspension;

    return Column(
      children: [
        Offstage(
          offstage: offsatge,
          child: Container(
            padding: const EdgeInsets.only(left: 15),
            alignment: Alignment.centerLeft,
            height: 40,
            child: Text(
              item.getSuspensionTag(),
              style: const TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: ListTile(
            tileColor: Colors.grey[200],
            minVerticalPadding: 20,
            title: Text(item.universityModel.name),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => UniversityDetailsScreen(
                      universityModel: item.universityModel),
                ),
              );
            },
          ),
        )
      ],
    );
  }

  void initItems() async {
    List<UniversityModel> items =
        await Provider.of<HomeScreenViewModel>(context, listen: false)
            .fetchUniversityList();
    universitiesData =
        items.map((e) => University(e, e.name[0].toUpperCase())).toList();
    SuspensionUtil.sortListBySuspensionTag(universitiesData);
    SuspensionUtil.setShowSuspensionStatus(universitiesData);
  }
}
